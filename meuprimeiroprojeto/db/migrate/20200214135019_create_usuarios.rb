class CreateUsuarios < ActiveRecord::Migration[6.0]
  def change
    create_table :usuarios do |t|
      t.string :nome_usuario
      t.string :matricula_usuario
      t.string :tipo_usuario
      t.string :email_usuario
      t.string :senha_usuario

      t.timestamps
    end
  end
end

class CreateTurmas < ActiveRecord::Migration[6.0]
  def change
    create_table :turmas do |t|
      t.string :nome_turma
      t.string :dias_da_semana
      t.time :horario_inicio
      t.time :horario_termino
      t.integer :carga_horaria_minima
      t.integer :carga_horaria_maxima
      t.integer :capacidade

      t.timestamps
    end
  end
end

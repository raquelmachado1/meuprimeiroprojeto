json.extract! turma, :id, :nome_turma, :dias_da_semana, :horario_inicio, :horario_termino, :carga_horaria_minima, :carga_horaria_maxima, :capacidade, :created_at, :updated_at
json.url turma_url(turma, format: :json)

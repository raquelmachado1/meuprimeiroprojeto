json.extract! usuario, :id, :nome_usuario, :matricula_usuario, :tipo_usuario, :email_usuario, :senha_usuario, :created_at, :updated_at
json.url usuario_url(usuario, format: :json)

require "application_system_test_case"

class TurmasTest < ApplicationSystemTestCase
  setup do
    @turma = turmas(:one)
  end

  test "visiting the index" do
    visit turmas_url
    assert_selector "h1", text: "Turmas"
  end

  test "creating a Turma" do
    visit turmas_url
    click_on "New Turma"

    fill_in "Capacidade", with: @turma.capacidade
    fill_in "Carga horaria maxima", with: @turma.carga_horaria_maxima
    fill_in "Carga horaria minima", with: @turma.carga_horaria_minima
    fill_in "Dias da semana", with: @turma.dias_da_semana
    fill_in "Horario inicio", with: @turma.horario_inicio
    fill_in "Horario termino", with: @turma.horario_termino
    fill_in "Nome turma", with: @turma.nome_turma
    click_on "Create Turma"

    assert_text "Turma was successfully created"
    click_on "Back"
  end

  test "updating a Turma" do
    visit turmas_url
    click_on "Edit", match: :first

    fill_in "Capacidade", with: @turma.capacidade
    fill_in "Carga horaria maxima", with: @turma.carga_horaria_maxima
    fill_in "Carga horaria minima", with: @turma.carga_horaria_minima
    fill_in "Dias da semana", with: @turma.dias_da_semana
    fill_in "Horario inicio", with: @turma.horario_inicio
    fill_in "Horario termino", with: @turma.horario_termino
    fill_in "Nome turma", with: @turma.nome_turma
    click_on "Update Turma"

    assert_text "Turma was successfully updated"
    click_on "Back"
  end

  test "destroying a Turma" do
    visit turmas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Turma was successfully destroyed"
  end
end
